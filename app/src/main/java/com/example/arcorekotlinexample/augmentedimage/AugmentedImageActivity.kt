package com.example.arcorekotlinexample.augmentedimage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.ar.sceneform.ux.ArFragment
import com.example.arcorekotlinexample.R
import com.google.ar.core.*
import com.google.ar.sceneform.FrameTime
import kotlinx.android.synthetic.main.activity_main.*

class AugmentedImageActivity : AppCompatActivity() {
    lateinit var arFragment: ArFragment
    private val augmentedImageMap : MutableMap<AugmentedImage, AugmentedImageNode> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        arFragment = supportFragmentManager.findFragmentById(R.id.ux_fragment) as ArFragment
        arFragment.arSceneView.scene.addOnUpdateListener{ onUpdateFrame(it)}

    }

    override fun onResume() {
        super.onResume()
        if (augmentedImageMap.isEmpty()) {
            imFitToScan.visibility = View.VISIBLE
        }
    }

    private fun onUpdateFrame(frameTime: FrameTime){
        val frame = arFragment.arSceneView.arFrame
        if(frame == null || frame.camera.trackingState != TrackingState.TRACKING){
            return
        }

        val updatedAugmentedImages : Collection<AugmentedImage> = frame.getUpdatedTrackables(AugmentedImage::class.java)

        for (augmentedImage in updatedAugmentedImages){

            when(augmentedImage.trackingState){
                TrackingState.PAUSED -> {
                    var text = "Detected Image " + augmentedImage.index
                    Toast.makeText(applicationContext,text, Toast.LENGTH_SHORT ).show()
                }
                TrackingState.TRACKING -> {
                    // Have to switch to UI Thread to update View.
                    imFitToScan.visibility = View.GONE

                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        var node =  AugmentedImageNode(this)
                        node.setImage(augmentedImage)
                        augmentedImageMap.entries
                        augmentedImageMap.put(augmentedImage, node)
                        arFragment.arSceneView.scene.addChild(node)
                    }
                }
                TrackingState.STOPPED -> {
                    augmentedImageMap.remove(augmentedImage)
                }

                else -> {}
            }
        }
    }
}
